﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class PlayingPiece
    {
        public readonly string name;
        public int pointValue;
        public bool isBlack;
        public bool hasMoved;
        public PlayingPiece(bool isblackcolour)
        {
            isBlack = isblackcolour;
        }
        protected PlayingPiece()
        {

        }
        public int myAge() { return 5; }
    }
}
