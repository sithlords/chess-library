﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class Queen: PlayingPiece
    {
        //foo(int a)
        //foo (int b)
        //foo (int a , int b)
        //foo(int b,bool a)
        //foo(bool a)
        public bool TryMove(int StartingRow, int StartingColumn, int TargetRow, int TargetColumn)
        {
            int AbsoluteRowDifference = Math.Abs(StartingRow - TargetRow);
            int AbsoluteColumnDifference = Math.Abs(StartingColumn - TargetColumn);

            if (AbsoluteColumnDifference == AbsoluteRowDifference)
            {
                return true;
            }
            if(StartingColumn ==TargetColumn)
            {
                return true;
            }
            if(StartingRow==TargetRow)
            {
                return true;
            }
            return false;
        }
       
    }
}
