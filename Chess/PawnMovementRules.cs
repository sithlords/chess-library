﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class PawnMovementRules : PlayingPiece
    {
        public readonly int StartingRow;

        public PawnMovementRules(bool isblackcolour) : base(isblackcolour)
        {
            if (isBlack)
            {
                StartingRow = 6;
            }
            else
            {
                StartingRow = 1;
            }
        }
        //6,2=>5.2
        public bool TryMove(int originRow, int originColumn, int targetRow, int targetColumn)
        {
            int diafora = originRow - targetRow;
            if (originColumn != targetColumn)
            {
                return false;
            }
            if (isBlack)
            {
                if (diafora < 3 && diafora > 0)
                {
                    if (diafora == 1)
                    {
                        return true;
                    }
                    if (diafora == 2 && originRow == StartingRow)
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                if (diafora > -3 && diafora < 0)
                {
                    if (diafora == -1)
                    {
                        return true;
                    }
                    if (diafora == -2 && originRow == StartingRow)
                    {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        //public void moveForward(bool doubleMove = false)
        //{
        //    if (isBlack)
        //    {
        //    }
        //    else
        //    {
        //    }
        //    Console.WriteLine("kalimera");
        //}
    }
}

