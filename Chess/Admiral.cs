﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Admiral: PlayingPiece
    {
       public bool TryMove(int originRow, int originColumn, int targetRow, int targetColumn)
        {
            

           int diaforaRow = Math.Abs(originRow - targetRow);
           int diaforaColumn = Math.Abs(originColumn - targetColumn);

            if (diaforaColumn == diaforaRow)
            {
                return true;
            }
            return false;
        }
    }
}
