﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    public class Tower: PlayingPiece
    {
        //private Tower():base()
        //{
        //}
        /// <summary>
        /// elegxi an ginete na kunithis
        /// </summary>
        /// <param name="originRow">arxiki grami</param>
        /// <param name="originColumn">arxiki stili</param>
        /// <param name="targetRow">teliki grami</param>
        /// <param name="targetColumn">teliki stili</param>
        /// <returns></returns>
        public bool TryMove(int originRow, int originColumn, int targetRow, int targetColumn)
        {
            if (originRow==targetRow)
            {
                return true;
            }
            if (originColumn == targetColumn)
            {
                return true;
            }
            return false;
        }
    }
}
