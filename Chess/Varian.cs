﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class Varian: PlayingPiece
    {
        public bool TryMove (int StartingRow, int StartingColumn, int TargetRow, int TargetColumn)
        {
            int diaforaRow = Math.Abs(StartingRow - TargetRow);
            int diaforaColumn = Math.Abs(StartingColumn - TargetColumn);

            if (diaforaRow<=1 && diaforaColumn<=1)
            {
                return true;
            }
            return false;
        }
    }
}
