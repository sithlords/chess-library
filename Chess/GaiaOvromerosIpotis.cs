﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class GaiaOvromerosIpotis : PlayingPiece
    {
       public bool TryMove(int StartingRow, int StartingColumn, int TargetRow, int TargetColumn)
        {
            int DiafRow = Math.Abs(StartingRow - TargetRow);
            int DiafColumn = Math.Abs(StartingColumn - TargetColumn);

            if (DiafRow == 1 && DiafColumn == 2)
            {
                return true;
            }
            if (DiafRow == 2 && DiafColumn == 1)
            {
                return true;
            }
            return false;

        }
    }
}
